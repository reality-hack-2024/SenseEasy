using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class SceneController : MonoBehaviour
{
    public Material newMaterial; // The new material to apply
    public GameObject[] objectsToExclude; // Array of GameObjects to exclude from material changes
    public Light[] lightsToToggleOff; // Array of lights to be turned off
    public Light spotlightToStayOn; // The spotlight that should stay on
    public float spotlightIntensityOn = 1.5f; // Desired intensity of the spotlight when on
    private float originalSpotlightIntensity; // To store the original intensity of the spotlight
    public Color spotlightSoftColor = new Color(1f, 0.5f, 0f); // Soft orange color
    private int pressCount = 0; // Counter for button presses
    private Dictionary<Renderer, Material> originalMaterials; // To store original materials

    void Start()
    {
        originalMaterials = new Dictionary<Renderer, Material>();
        StoreOriginalMaterials();

        // Store the original intensity of the spotlight
        if (spotlightToStayOn != null)
        {
            originalSpotlightIntensity = spotlightToStayOn.intensity;
        }
    }

    void StoreOriginalMaterials()
    {
        Renderer[] renderers = FindObjectsOfType<Renderer>();
        foreach (Renderer rend in renderers)
        {
            if (!IsRendererExcluded(rend))
            {
                originalMaterials[rend] = rend.material;
            }
        }
    }

    bool IsRendererExcluded(Renderer renderer)
    {
        foreach (var obj in objectsToExclude)
        {
            if (obj != null && renderer.gameObject == obj)
            {
                return true;
            }
        }
        return false;
    }

    public void OnButtonPressed()
    {
        pressCount++; // Increment the press counter
        ApplyMaterialChange(pressCount % 2 == 0); // Apply changes based on even/odd press count
    }

    void ApplyMaterialChange(bool revertToOriginal)
    {
        foreach (var pair in originalMaterials)
        {
            var rend = pair.Key;
            if (rend != null)
            {
                rend.material = revertToOriginal ? pair.Value : newMaterial;
            }
        }

        ToggleLights(!revertToOriginal);
    }

    void ToggleLights(bool toggle)
    {
        // Toggle the specific spotlight
        if (spotlightToStayOn != null)
        {
            spotlightToStayOn.intensity = toggle ? spotlightIntensityOn : originalSpotlightIntensity;
            spotlightToStayOn.color = spotlightSoftColor;
        }

        // Toggle other lights off/on
        foreach (Light light in lightsToToggleOff)
        {
            if (light != null)
            {
                light.enabled = !toggle;
            }
        }
    }
}
