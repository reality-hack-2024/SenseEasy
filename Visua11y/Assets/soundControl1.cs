using UnityEngine;
using UnityEngine.UI;

public class AudioControl : MonoBehaviour
{
    public Slider volumeSlider; // The slider to control the volume
    public AudioSource audioSource1; // The first audio source
    public AudioSource audioSource2; // The second audio source

    void Start()
    {
        if (volumeSlider != null)
        {
            // Add a listener to the slider to call the OnSliderValueChanged method when the value changes
            volumeSlider.onValueChanged.AddListener(OnSliderValueChanged);
        }
    }

    void OnSliderValueChanged(float value)
    {
        if (audioSource1 != null && audioSource2 != null)
        {
            // Set the volume of audioSource1 to the slider value
            audioSource1.volume = value;

            // Set the volume of audioSource2 to 1 minus the slider value
            audioSource2.volume = (1 - value) * 5;
        }
    }
}
